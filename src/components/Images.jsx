import { useEffect, useState } from "react";
import "/src/styles/Image.css";
import Name from "./Name";

const Images = (props) => {
  const [sound, setSound] = useState(new Audio(props.imgData.sound));
  const [playing, setPlaying] = useState(false);

  const onClick = (e) => {
    e.preventDefault();

    if (!playing) {
      setPlaying((playing) => true);
      sound.play();
      sound.volume = props.volume;
      if (!props.isClicked) props.registerClick();
    } else {
      setPlaying((playing) => false);
      sound.pause();
      sound.currentTime = 0;
    }
  };

  useEffect(() => {
    if (props.isClicked) {
      const to = setTimeout(() => {
        props.registerClick();
        setPlaying((playing) => false);
      }, (props.clickDelay + (playing ? sound.duration : 0)) * 1000);

      return () => clearTimeout(to);
    }
  }, [props.isClicked, playing]);

  useEffect(() => {
    setSound((sound) => new Audio(props.imgData.sound));
  }, [props.imgData]);

  return (
    <div className="imageContainer" onClick={(e) => onClick(e)}>
      <img className="image" src={props.imgData.src} />
      {props.isClicked &&
        props.imgData.names.map((name_data) => (
          <Name key={name_data.name} nameData={name_data}></Name>
        ))}
      {!props.isClicked && props.helper && (
        <div className="helper">
          <label className="helper-label">Trykk på meg!</label>
        </div>
      )}
    </div>
  );
};

export default Images;
