import { useEffect, useState } from "react";
import "./styles/App.css";
import * as myData from "./data.json";
import Images from "./components/Images";

const App = () => {
  const jason = myData.default;
  const [imgIndex, setImgIndex] = useState(0);
  const [clicked, setClicked] = useState(false);

  const imgs = jason.bilder;

  useEffect(() => {
    const i = setInterval(() => {
      if (!clicked) {
        setImgIndex((imgIndex) => (imgIndex + 1) % imgs.length);
      }
    }, jason.img_time * 1000);
    return () => clearInterval(i);
  }, [clicked]);

  return (
    <div>
      <Images
        imgData={imgs[imgIndex]}
        clickDelay={jason.text_time}
        volume={jason.volume}
        helper={jason.helperText}
        registerClick={() => setClicked((clicked) => !clicked)}
        isClicked={clicked}
      ></Images>
    </div>
  );
};

export default App;
