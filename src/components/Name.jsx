import "/src/styles/Name.css";

const Name = (props) => {
  return (
    <div
      className="TextContainer"
      style={{
        left: props.nameData.position.x * 100 + "%",
        top: props.nameData.position.y * 100 + "%",
      }}
    >
      <label className="Text">{props.nameData.name}</label>
    </div>
  );
};

export default Name;
